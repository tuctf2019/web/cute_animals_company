CREATE DATABASE challenge;
CREATE TABLE users (user VARCHAR(20) NOT NULL, password VARCHAR(50) NOT NULL);
INSERT INTO users VALUES ('dave', 'hunter2');
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'fun new password times';

Add extension=php_mysqli.dll to /etc/php/7.2/apache2/php.ini
