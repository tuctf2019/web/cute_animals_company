# Cute Animals Company

This multi-part chal has a little bit of cookie time, a decently difficult SQLi, and some directory transversal (with all new filtering)

* Desc: Look at this cute website! Why don't you find some cute animals that are hidden from view?
* Flag: `TUCTF{m0r3_cut3_4n1m415_c4n_b3_f0und_4t_https://bit.ly/1HU2m5Q}`

## How To:
1. So you see adorable animals and an admin login in the menu...but it doesn't work. You see the cookie `allowed`. It's current value is Base64 of `false`
2. Change that cookie value to Base64 of `true` (regardless of case) and you're golden.
3. Now you see a login page. This is another SQLi (that does have error messaging turned on). It's a pretty simple union SQLi "`' UNION SELECT user, password FROM users --  `" (the user and password fields can be gained by realizing that the same person made all these login pages and you get the query in Login to Access...or just guessing it's a pretty common field scheme)
4. This returns results `dave`/`hunter2`, login with these
5. Now you have a box to put words into. If you type anything into the search field you'll see `?file=` which should hint to you this is local file inclusion/directory transversal. So you try the classic `../../../etc/passwd`...and nothing happens
6. You realize that the results of the query are being printed below the search box every time...but there are no `../` in the above query. There's some filter protection! Now here's where it gets fun. The protection is only one level deep. So `....//` will produce `../` (which you see when it's printed below the query box). Use this knowledge to get to `etc/passwd` (one such result is `..././....//..././etc/passwd`) and you'll find the flag at the bottom of the `/etc/passwd`
